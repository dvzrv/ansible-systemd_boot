<!--
SPDX-FileCopyrightText: 2023 David Runge <dvzrv@archlinux.org>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# archlinux.systemd_boot

An [Ansible](https://www.ansible.com/) role to configure systemd-boot configuration and install systemd-boot to EFI.

## Requirements

An Arch Linux system or an Arch Linux chroot.

This role requires existing initrd images to be present (e.g. built by [mkinitcpio](https://gitlab.archlinux.org/dvzrv/ansible-mkinitcpio) or dracut).
The initrds are expected to be located in `/boot` (unless `systemd_boot.esp_path` specifies another location).

## Role Variables

By default this role uses a very generic configuration, that has no knowledge about a specific distribution:

* `systemd_boot`: a dict to define all configuration for systemd-boot (defaults to unset)
  * `esp_path`: a string defining the directory to which the EFI System Partition (ESP) is mounted (defaults to `/boot`). **NOTE**: Separate partitions for boot and ESP are not supported.
  * `default`: a (glob-capable) string of the configuration file to select as default during boot (defaults to `arch-linux-linux.conf`). **NOTE**: The string `@saved` has special meaning and if set will lead to the selected entry to be saved as EFI variable and reused upon next boot.
  * `timeout`: a non-negative integer setting the seconds to wait before booting the selected entry (defaults to `5`)
  * `console_mode`: a string describing the console mode (defaults to `keep` - one of `0`, `1`, `2`, `auto`, `max` or `keep` is possible)
  * `editor`: a boolean indicating whether to allow editing of entries (defaults to `true`)
  * `auto_entries`: a boolean indicating whether to show other entries found on the boot device (defaults to `true`)
  * `auto_firmware`: a boolean indicating whether to generate an entry for rebooting to firmware (defaults to `true`)
  * `beep`: a boolean indicating whether to beep each second of the configured `timeout` or not (defaults to `false`)
  * `prefix_pretty`: a string that defines the prefix for the pretty printed entry in the bootloader menu (defaults to `Arch Linux`)
  * `prefix_file`: a string that defines the prefix for the files in the `/boot/loader/entries/` directory (defaults to `arch-linux`)
  * `auto_update`: a boolean indicating whether to enable `systemd-boot-update.service` (defaults to `false`)
  * `specification_type`: a non-negative integer defining which [Boot Loader Specification Type](https://uapi-group.org/specifications/specs/boot_loader_specification/) to use (defaults to `1`). **NOTE**: Currently only Boot Loader Specification Type #1 is supported!

The following variables have no defaults in this role, but are available to override behavior and use special functionality:

* `bootloader_type`: a string defining which type of bootloader to install. **NOTE**: For systemd-boot only `uefi` is supported.
* `chroot_dir`: the directory to chroot into, when including `chroot.yml` tasks (defaults to unset)
* `kernel_cmdline`: a list of strings representing kernel cmdline options, which are added to each bootloader entry (defaults to `[]`)
* `microcode`: an optional string defining which microcode - one of `amd` or `intel` - to preload (defaults to unset)

## Dependencies

* [archlinux.mkinitcpio](https://gitlab.archlinux.org/dvzrv/ansible-mkinitcpio) for configuring and building custom initrds

## Example Playbook

```yaml
- name: Set linux-hardenend as default with timeout of 3 seconds
  hosts: my_hosts
  vars:
    systemd-boot:
      default: arch-linux-linux-hardened.conf
      timeout: 3
    kernel_cmdline:
      - audit=1
    microcode: intel
  tasks:
    - name: Include archlinux.systemd_boot
      ansible.builtin.include_role:
        name: archlinux.systemd_boot
```

```yaml
- name: Set linux-hardenend as default with timeout of 3 seconds in a chroot
  hosts: my_hosts
  vars:
    chroot_dir: /
    systemd-boot:
      default: arch-linux-linux-hardened.conf
      timeout: 3
    kernel_cmdline:
      - audit=1
    microcode: intel
  tasks:
    - name: Include archlinux.systemd_boot for chroot
      ansible.builtin.include_role:
        name: archlinux.systemd_boot
      tasks_from: chroot
      handlers_from: chroot
```

## License

GPL-3.0-or-later

## Author Information

David Runge <dvzrv@archlinux.org>
